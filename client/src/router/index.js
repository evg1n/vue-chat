import Vue from 'vue'
import Router from 'vue-router'
import VueSocketIO from 'vue-socket.io'
import ClientHome from '@/components/ClientHome'

Vue.use(Router)
Vue.use(new VueSocketIO({
  connection: 'http://localhost:8000'
})
)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ClientHome',
      component: ClientHome
    }
  ]
})
